
#Libraries Imported
import cv2
import os

# Using Trained data set -haarcascade
TrainingPath=os.path.dirname(cv2.__file__)+"/data/haarcascade_frontalface_default.xml"


faceDetect = cv2.CascadeClassifier(TrainingPath)

captureVideo = cv2.VideoCapture(0)

while True:
    # Capture video frame-by-frame
    ret, frames = captureVideo.read()
    
    gray = cv2.cvtColor(frames, cv2.COLOR_BGR2GRAY)

    faces = faceDetect.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )
    # Draw a rectangle around the faces
    for (x, y, w, h) in faces:
        cv2.rectangle(frames, (x, y), (x+w, y+h), (0, 255, 0), 2)
    # Display the resulting frame
    cv2.imshow('Video', frames)
    #Press 'q' to quit and close the program
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

captureVideo.release()
#Close video pop up windows
cv2.destroyAllWindows()


